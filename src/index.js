const keyboard = require('./keyboard'),
    numberKeyboard = require('./keyboard.number');

window.aKeyboard = {
    keyboard,
    numberKeyboard
}
